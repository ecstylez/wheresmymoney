<?php

use Core\Controller;

/**
 * Main Controller
 */
class MainController extends Controller
{
	public function index()
	{
        $params = [
            'title'             => 'title here',
            'upcoming_payments' => [
                'columns' => [
                    'date',
                    'tenant',
                    'amount due' => [
                        'class' => 'right-align',
                    ],
                ],
                'data' => [
                    [
                        'tenant'         => 'Chad Wray',
                        'payment_date'   => '12/29/2018',
                        'payment_amount' => '$749.00',
                    ],
                    [
                        'tenant'         => 'Mike Wray',
                        'payment_date'   => '12/30/2018',
                        'payment_amount' => '$249.00',
                    ],
                ],
            ],
            'tenants' => [
                'columns' => [
                    'tenant names',
                ],
                'data' => [
                    [
                        'tenant' => 'Chad Wray',
                    ],
                    [
                        'tenant' => 'Mike Wray',
                    ],
                    [
                        'tenant' => 'Alex Berhane',
                    ],
                    [
                        'tenant' => 'Kenya Raphael',
                    ],
                ],
            ],
            'properties' => [
                'columns' => [
                    'address',
                    'city',
                    'state',
                    'zipcode',
                ],
                'data' => [
                    [
                        'address' => '14030 Harpers Ferry St',
                        'city'    => 'Davie',
                        'state'   => 'FL',
                        'zipcode' => '33325',
                    ],
                    [
                        'address' => '75 NE 211 ST',
                        'city'    => 'Miami',
                        'state'   => 'FL',
                        'zipcode' => '33179',
                    ],
                    [
                        'address' => '16500 Golf Club Rd, Apt 313',
                        'city'    => 'Weston',
                        'state'   => 'FL',
                        'zipcode' => '33326',
                    ],
                ],
            ]
        ];
		$this->render('main/index', $params);
	}
}