const gulp   = require('gulp'),
    concat   = require('gulp-concat'),
    uglify   = require('gulp-uglify'),
    sass     = require('gulp-sass'),
    debug    = require('gulp-debug'),
    del      = require('del');

const config = {
    sass: {
        src: 'dist/scss/**/*.scss',
        dest: 'public/dist/css'
    },
    js: {
        src: 'dist/js/**/*.js',
        dest: 'public/dist/js'
    }
};

function watchSass() {
    gulp.watch(config.sass.src, buildSass);
}

function buildSass() {
    return gulp.src(config.sass.src)
        .pipe(concat('main.min.css'))
        .on("error", sass.logError)
        .pipe(sass())
        .pipe(gulp.dest(config.sass.dest));
}

gulp.task('watch', gulp.series(watchSass));
gulp.task('default', gulp.series(buildSass));