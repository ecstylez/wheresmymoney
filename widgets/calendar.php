<?php

use Core\Controller;

/**
 * Calendar Widget
 */
class Calendar extends Controller implements Widget
{

    private $calendarVars = [];
    private $template     = '';

    public function setVariables()
    {
        $date = array_values(getdate());
        list(
            $this->calendarVars['second'],
            $this->calendarVars['minute'],
            $this->calendarVars['hour'],
            $this->calendarVars['day'],
            $this->calendarVars['weekday'],
            $this->calendarVars['month'],
            $this->calendarVars['year'],
            $this->calendarVars['yearday'],
            $this->calendarVars['weekday'],
            $this->calendarVars['monthtitle'],
            $this->calendarVars['unix']
        ) = $date;

        $this->calendarVars['days_in_week']       = 7;
        $this->calendarVars['days_in_month']      = date('t', $this->calendarVars['unix']);
        $this->calendarVars['days']               = [];
        $this->calendarVars['first_day_of_month'] = date('w', strtotime($this->calendarVars['year'] . '-' . $this->calendarVars['month'] . '-01'));
    }

    public function buildWidget()
    {
        // get all necessary variables
        $this->setVariables();

        // format days and positions
        $monthDays = $this->formatMonthDays();
        $dayCount  = 0;

        // $this->template .= '<div class="card">';
        $this->template .= $this->monthContainer();
        // '<h3>' . date('F', strtotime($this->calendarVars['unix'])) . '</h3>';

        $this->template .= '<table class="white grey-text centered">';

        // get day labels
        $this->template .= '<tr class="pink lighten-2 white-text">';
        for ($i=0; $i < $this->calendarVars['days_in_week']; $i++) {
            $width = ($i == 0) ? 14.32 : 14.28;
            $this->template .= '<th style="width:' . $width . '%;">' . JDDayOfWeek($i-1,1) . '</th>';
        }
        $this->template .= '</tr>';

        // iterate over days and display them
        foreach ($monthDays as $key => $day) {
            if ($key % $this->calendarVars['days_in_week'] == 0) {
                $this->template .= '<tr>';
            }

            $this->template .= '<td>' . $day . '</td>';

            if ($key % $this->calendarVars['days_in_week'] == 7) {
                $this->template .= '</tr>';
            }
        }

        $this->template .= '</table>';
        // $this->template .= '</div>';


    }

    public function printWidget()
    {
        echo $this->template;
    }

    private function formatMonthDays()
    {
        $pre_days = [];

        for ($i = 0; $i < $this->calendarVars['first_day_of_month']; $i++) {
            $pre_days[] = '';
        }
        $days = range(1, $this->calendarVars['days_in_month']);

        return array_merge($pre_days,$days);
    }

    public function monthContainer()
    {
        // $container = '<h3>' . date('F', strtotime($this->calendarVars['unix'])) . '</h3>';
        $container = "";
        return $container;
    }
}
