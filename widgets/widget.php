<?php

/**
 * Widget Class
 */
interface Widget {

    public function setVariables();

    public function buildWidget();

    public function printWidget();
}
