<?php

use Core\Controller;

/**
 * List Class
 */
class LineList extends Controller implements Widget
{

    private $template = '';
    private $columns  = [];
    private $data     = [];

    public function __construct($vars = [])
    {
        $this->data = $vars;
    }
    public function setVariables($columns = [], $data = [])
    {
        // set columns
        $this->columns = $columns;

        // set data
        $this->data = $data;
    }

    public function buildWidget()
    {
        $this->template = "<table class='highlight'>
            <thead>
                <tr>" . $this->formatColumns() . " </tr>
            </thead>
        ";

        foreach ($this->data as $row) {
            $this->template .= "<tbody>";
            $this->template .= "<tr>";
            $this->template .= $this->formatRow($row);
            $this->template .= "</tr>";
            $this->template .= "</tbody>";
        }

        $this->template .= "</table>";
    }

    public function printWidget()
    {
        echo $this->template;
    }

    private function formatColumns()
    {
        $columns = [];

        foreach ($this->columns as $key => $column) {
            if (is_numeric($key)) {
                $columns[] = "<th>" . $this->formatHeader($column) . "</th>";
            } else {
                $columns[] = "<th " . $this->formatAttributes($column) . ">" . $this->formatHeader($key) . "</th>";
            }
        }
        return implode("",$columns);
    }

    private function formatRow($rowData = [])
    {
        $columns  = [];
        $rowStyle = array_values($this->columns);
        $rowData  = array_values($rowData);

        for ($i = 0; $i < count($rowData); $i++) {
            $attributes = (is_array($rowStyle[$i])) ? $this->formatAttributes($rowStyle[$i]) : "";
            $columns[]  = "<td " . $attributes . ">" . $rowData[$i] . "</td>";
        }

        return implode("", $columns);
    }

    private function formatAttributes($params = [])
    {

        $attributes = [];

        foreach ($params as $attribute => $value) {
            $attributes[] = $attribute . " = '" . $value . "'";
        }

        return implode(" ", $attributes);
    }

    private function formatHeader($string = "")
    {
        return ucwords(str_replace("_"," ", $string));
    }
}
