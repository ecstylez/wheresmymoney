<?php

// init
require_once __DIR__ . '/../init.php';

$loader = new Autoloader();
$params = [];
$index  = "index";

if (isset($_GET['path'])) {
    $pieces     = explode("/", $_GET['path']);
    $controller = $pieces[0];
    $action     = isset($pieces[1]) && !empty($pieces[1]) ? $pieces[1] : $index;

    unset($pieces[0]);
    unset($pieces[1]);

    $params = $pieces;
} else {
    $controller = "Main";
    $action     = $index;
}

// format callers
$className = $controller . "Controller";
(new $className())->$action($params);

