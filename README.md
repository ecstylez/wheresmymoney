
# Where's My Money?

## What is it?
Just a fun little tool that i'm building which may (or may not) be used. It's basically a fun little tool to track payments and send reminders to your tenant(s) when payments are due.

## Is it completed?
Not yet, but feel free to follow my progress as i work on it over time :)

## Author
Mark Eccleston
Software Engineer
Atlanta, GA

[m.ecstylez@gmail.com](m.ecstylez@gmail.com)

