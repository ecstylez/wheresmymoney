<?php

class Autoloader {
    public function __construct()
    {
        $this->loadDependencies();

        spl_autoload_register([$this, "loader"]);
    }

    private function loadDependencies()
    {
        $this->loadController();
        // $this->loadView();
        $this->loadEnv();
    }

    private function loader($class)
    {
        $dir   = $this->checkClass($class);
        require_once(__DIR__ . '/' . $dir . '/' . $class . '.php');
    }

    private function checkClass($class)
    {
        $dir           = "controllers";
        $coreClasses   = ['Controller', 'Model'];

        if (in_array($class, $coreClasses)) {
            $dir = "core";
        } elseif ($class == "Widget") {
            $dir = "widgets";
        }

        return $dir;
    }

    private function loadController()
    {
        require_once(__DIR__ . '/core/Controller.php');
    }

    // private function loadView()
    // {
    //     require_once(__DIR__ . '/core/View.php');
    // }

    private function loadEnv()
    {
        require_once(__DIR__ . '/core/Env.php');
        new \Core\Env(__DIR__ . '/.env');
    }

}