<?php

namespace Core;

/**
 * ENV class
 */
class Env
{

	private $filePath;

	public function __construct($filePath)
	{
		$this->filePath = $filePath;
		$this->load();
	}

	private function load()
	{
		if (!is_readable($this->filePath) || is_file($this->filePath)) {
			$filePath = $this->filePath;
			$lines    = file($filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

			foreach ($lines as $line) {
				$this->setEnvironmentVariable($line);
			}
		}
	}

	private function setEnvironmentVariable($name)
	{
		list($name,$value) = explode("=", $name);
		apache_setenv($name, $value);
		putenv("$name=$value");
		$_ENV[$name]       = $value;
		$_SERVER[$name]    = $value;
	}

	public function getEnv($name)
	{
		switch(true) {
			case array_key_exists($name, $_ENV):
				return $_ENV[$name];
			case array_key_exists($name, $_SERVER):
				return $_SERVER[$name];
			default:
				$value = getenv($name);
				return $value === false ? null : $value;
		}
	}
}