<?php

namespace Core;

/**
 * Controller parent class
 */
class Controller
{
    private $controllerName;
    private $viewName;
    public  $root;

    function __construct()
    {
        $this->appRoot = __DIR__ . '/../';
    }

    public function render($page, $data = [])
    {
        list($directory, $action) = explode("/", $page);
        $this->controllerName     = $directory;
        $this->viewName           = $action;

        // var_export($params, true);
        require_once(__DIR__ . '/../templates/inc/header.php');
        require_once(__DIR__ . '/../views/' . $this->controllerName . '/' . $this->viewName . '.php');
        require_once(__DIR__ . '/../templates/inc/footer.php');
    }

    public function debug($content = '')
    {
        $content = var_export($content, true);
        $template = <<<EOD
            <div class='yellow black-text' style='padding:10px;'>
                <pre>{$content}</pre>
            </div>
EOD;
        echo $template;
    }
}