<?php
    require_once($this->appRoot . 'widgets/list.php');
    require_once($this->appRoot . 'widgets/calendar.php');
?>
<div class="col l3">
    <div class="row">
        <div class="col l12">
            <div class="card card-widget">
                <div class="card-content white-text cyan lighten-1">
                    <span>Upcoming Payments</span>
                </div>
                <div class="card-content widget-content">
                    <?php
                        $payments_list = new LineList();
                        $payments_list->setVariables($data['upcoming_payments']['columns'], $data['upcoming_payments']['data']);
                        $payments_list->buildWidget();
                        $payments_list->printWidget();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col l12">
            <div class="card card-widget">
                <div class="card-content white-text red lighten-1">
                    <span>Tenants</span>
                </div>
                <div class="card-content widget-content">
                    <?php
                        $tenants = new LineList();
                        $tenants->setVariables($data['tenants']['columns'], $data['tenants']['data']);
                        $tenants->buildWidget();
                        $tenants->printWidget();
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col l12">
            <div class="card card-widget">
                <div class="card-content white-text orange lighten-1">
                    <span>Properties</span>
                </div>
                <div class="card-content widget-content">
                    <?php
                         $properties = new LineList();
                         $properties->setVariables($data['properties']['columns'], $data['properties']['data']);
                         $properties->buildWidget();
                         $properties->printWidget();
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col l9">
    <div class="card calendar-widget">
        <div class="card-content center-align pink white-text">
            <h4>January 2019</h4>
        </div>
        <div class="card-content widget-content">
            <?php
                $calendar = new Calendar();
                $calendar->buildWidget();
                $calendar->printWidget();
            ?>
        </div>
    </div>
</div>