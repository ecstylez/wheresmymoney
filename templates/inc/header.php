<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Document</title>
    <style type="text/css">
        /* google material icons */
        @import "https://fonts.googleapis.com/icon?family=Material+Icons";

        /* roboto font */
        @import "https://fonts.googleapis.com/css?family=Roboto";

        /* materialize */
        @import "/dist/css/materialize.min.css";
        @import "/dist/css/main.min.css";
    </style>
</head>
<body class="grey lighten-5">
    <header>
        <div class="row">
            <nav class="light-blue">
                <div class="nav-wrapper">
                    <div class="col l12">
                        <a href="#" id="menu-btn" class="btn-floating btn-large waves-effect waves-light pink accent-2">
                            <i class="material-icons">menu</i>
                        </a>
                        <a href="#" class="brand-logo">Where's My Money?</a>
                        <div class="right">
                            <span><?= date('F d, Y'); ?></span>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <main>
        <div class="row">
